# macOS shared runners overview

The following document describes the architecture behind our macOS shared runners.

## Useful links

**Related repositories:**

- [`shared-runners/macos` repository](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos)
- [`shared-runners/images/macstadium/orka` repository](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka)
- [`custom-executor-drivers/autoscaler` repository](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler)
- [`charts/gitlab-runner` repository](https://gitlab.com/gitlab-org/charts/gitlab-runner)

**Documentation:**

- [Orka documentation @ MacStadium](https://orkadocs.macstadium.com/docs)
- [Orka API reference](https://documenter.getpostman.com/view/6574930/S1ETRGzt?version=latest)

**Operations:**

- [Orka Web UI](http://10.221.188.100/web/overview)
- [MacStadium support portal](https://portal.macstadium.com/)
- [Logicmonitor dashboard](https://macstadium.logicmonitor.com/)

## Architecture

<img src="./diagram/architecture.png" width="400" />

## How to access the environment

Go through the runbook [Preparing your local environment](./runbooks/preparing-your-local-environment.md)

## Troubleshooting

### Runbooks

A number of runbooks exist in the [./runbooks](./runbooks) directory:

- [Preparing your local environment](./runbooks/preparing-your-local-environment.md)
- [Add a new cluster](./runbooks/add-a-new-cluster.md)
- [Add a new project to orka-canary](./runbooks/add-a-new-project-to-orka-canary.md)
- [Add a new runner environment](./runbooks/add-a-new-runner-environment.md)
- [Deploying the Helm release](./runbooks/deploying-the-helm-release.md)
- [Onboarding new closed beta customer](./runbooks/onboarding-new-closed-beta-customer.md)
- [Update Prometheus node target](./runbooks/update-prometheus-target.md)

### List all customer VMs

By default, your user can only see the VMs you own. To list all the VMs in the cluster, do:

```shell
orka $CLUSTER vm list --filter all
```

### Connecting to the cluster with k9s

Execute the following:

```shell
k9s --kubeconfig ./kubeconfig-orka-$CLUSTER -n sandbox
```

### Getting a pod logs

To see the runner standard out or any other logs:

```shell
kubectl --kubeconfig=./kubeconfig-orka-$CLUSTER logs orka-beta-gitlab-runner-c6bdd6cdb-2jhwc -c orka-beta-gitlab-runner

...
Verifying runner... is alive                        runner=XXXXX
...
```

### Getting the autoscaler logs

The logs are stored in `/home/gitlab-runner/autoscaler.log`

```shell
kubectl --kubeconfig=./kubeconfig-orka-$CLUSTER exec orka-beta-gitlab-runner-6cf5c8db96-b68mg -- tail /home/gitlab-runner/autoscaler.log
```

### Connecting to a customer VM

The password for the `gitlab` user is randomized when the build start, but the autoscaler uses an SSH keypair to connect. In order to get access to the private key, you can run:

```shell
kubectl --kubeconfig=./kubeconfig-orka-$CLUSTER exec --stdin --tty orka-canary-gitlab-runner-78f59b6d65-z7dxt -- \
  cat /home/gitlab-runner/$VM_NAME.json | jq -r ".PrivateSSHKey" | base64 -d
```

Note that this JSON file is lost as the autoscaler pod gets rescheduled or as the customer job terminates.

### Accessing the LogicMonitor dashboard provided by MacStadium

The dashboard lives at https://macstadium.logicmonitor.com/

## Environments

We maintain four separate environments that map directly to Helm releases on our Orka Kubernetes cluster:

- `orka` is our production environment. These runners execute customer builds on dot com tagged with `shared-macos-amd64` (*as of Apr 1st this environment does not exist yet*)
- `orka-canary` is our canary environment. These runners execute internal builds with the `shared-macos-amd64-canary` tag and enable access to the `macos-next` image. Any production change is first deployed in this environment and verified.
- `orka-beta` is our closed-beta environment. While we transition towards open beta and the `orka` environment, these runners execute builds for legacy customers onboarded into the closed beta. It also works with the `shared-macos-amd64` tag. We will remove this environment once the transition is over.
- `gitlab-org-orka` is our Linux environment, used to run linux jobs inside the VPN, like building OS images in the [`orka`](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka) repository

## Orka and physical infrastructure

Orka is the name of the product we use from our provider MacStadium. It is an abstraction built on top of physical Mac Pros that allow us to spin up and down macOS VMs programmatically and host Kubernetes pods.

### Nodes

At the time of writing, we have an undisclosed number of physical Mac Pros available. Each Mac Pro has 24 cores and 62.75GB of RAM available.

### Network storage

A shared directory is made available to us to store OS images for our VMs. It is accessible via `orka-cli` image commands only. The storage has several limitations:

- We have a fixed quota of 4TB. We use it up very quickly since our images are 50+GB, and we build a few of them each CI pipeline. Please be mindful of storage use.
- Image filenames are limited to 29 chars, including extension.
- Storage operations (image rename, copy, commit, save) are not atomic
- Changing the base image of a running VM can corrupt the VM

## Kubernetes

Orka is based on Kubernetes and we get access to this Kubernetes environment and can run pods.

There are a [number of limitations to this Kubernetes environment](https://orkadocs.macstadium.com/docs/kubernetes-limitations-in-orka), namely access to a single `sandbox` namespace and a few blocked APIs and resources.

### Helm chart

We use the [official gitlab-runner helm chart](https://gitlab.com/gitlab-org/charts/gitlab-runner) to deploy the environment.

### Secrets

The following secrets exist in our cluster:

| Name                                   | Key                                | Description                                                                                   |
|----------------------------------------|------------------------------------|-----------------------------------------------------------------------------------------------|
| `orka-beta` (prod only)                | `api-endpoint`                     | Orka API endoint                                                                              |
|                                        | `api-token`                        | API token for the `autoscaler-orka-beta@gitlab.com` orka user                                 |
|                                        | `vm-password`                      | Production VM password                                                                        |
| `orka-canary` (prod only)              | `api-endpoint`                     | Orka API endoint                                                                              |
|                                        | `api-token`                        | API token for the `autoscaler-orka-canary@gitlab.com` orka user                               |
|                                        | `vm-password`                      | Production VM password                                                                        |
| `runner-gitlab-org-orka`               | `runner-token`                     | Token registered with the orka repo (linux k8s executor)                                      |
|                                        | `registration-token`               | "" (empty)                                                                                    |
| `runner-orka-beta` (prod only)         | `runner-token`                     | Instance runner token for production                                                          |
|                                        | `registration-token`               | "" (empty)                                                                                    |
| `runner-orka-canary` (prod only)       | `runner-token`                     | Token registered with the orka repo and personal playgrounds                                  |
|                                        | `registration-token`               | "" (empty)                                                                                    |
| `gitlab-allowlist-token`  (prod only)  | `GITLAB_TOKEN`                     | Gitlab token that can read https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos-allowlist |
| `gcs-runners-cache-credentials`        | `gcs-application-credentials-file` | Authentication file for runners to access shared cache in GCS                                 |

## VMs

Each customer job gets assigned a new VM dedicated to the job. We don't reuse VMs between jobs.

At the time of writing, we give each customer job a 4 cores / 10GB RAM virtual machine with password-less sudo access.

## Next image

We maintain an internal *alpha* image called `macos-next` that is continuously updated with the full provisioning playbook, starting from a clean install of macOS. It ensures our provisioning stays fresh, and we're able to build new images over time as dependencies change.

This image is only accessible on the `canary` runner.

### Playbooks

The [`orka`](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka) repository contains the ansible rules we use to maintain our images.

### Image aliasing

Since our image store is only a network mount, there is no guarantee of atomicity when manipulating images. On top of that, changing the base image of a running VM can corrupt the VM. So, the conclusion is we must never alter an image that may be in use in production, either by copying, moving, renaming, or overwriting.

To release an image to our customers safely, we maintain an alias system, where images built by the CI are immutable and referenced via a versioned name `ork-YYYYMMDDhhmm-$MACOS_VERSION+$XCODE_VERSION` (for example, `ork-202103201455-10.15+11`). This pattern follows the 29 char limit if we leave space for `10.X` macOS versions and `10+` Xcode versions, assuming we skip the seconds in the timestamp. Then, in our autoscaler configuration, those images are mapped to their public, user-friendly identifier `macos-$MACOS_VERSION-xcode-$XCODE_VERSION` (for example `macos-10.15-xcode-11`)

The `macos-next` image is a particular case. Since this is for internal use only and we want to update it continuously from the `master` branch,  we ignore the risk of storage consistency problems. To minimize it, we build it first under a temporary name then rename it `macos-next` (considering a rename is quicker than a copy).
