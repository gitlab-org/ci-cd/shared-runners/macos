# Onboarding a new customer into the closed beta

## Preparation

Go through the runbook [Preparing your local environment](./preparing-your-local-environment.md)

## Execution

1. Once the customer has filed his issue in the [macos-buildcloud-runners-beta](https://gitlab.com/gitlab-com/macos-buildcloud-runners-beta/-/issues) and has provided links to their group/projects, you can proceed with registration.

1. Add the customer to the allowlist at https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos-allowlist/-/blob/main/shared. The allowlist contains group and project prefixes:
   - A prefix of `/group/` would recursively allow any group/project that is a member of `/group`.
   - For a specific project, add the full project path, eg: `/group/subgroup/project`.

1. When the change is committed to the main branch of that repository, it usually takes 1-2min to become effective, without requiring a pod restart. A CronJob will pull the new data and write it to a volume that the runner is reading

1. Comment on the customer issue:

    > Hi USER
    >
    > Your beta macOS runner is online now! You should be able to execute builds by setting the following in your `.gitlab-ci.yml`:
    >
    > ```yaml
    > tags: [ shared-macos-amd64 ]
    > image: macos-12-xcode-13
    > ```
    >
    > We support the following images:
    >
    > ```yaml
    > image: macos-10.13-xcode-7
    > image: macos-10.13-xcode-8
    > image: macos-10.13-xcode-9
    > image: macos-10.14-xcode-10
    > image: macos-10.15-xcode-11
    > image: macos-11-xcode-12
    > image: macos-12-xcode-13
    > ```
    >
    > Let us know if you have any questions! I'll leave this issue open for future discussions.
    >
    > /unassign ME
    >
    > /label ~"closedbeta::v2"

1. Ensure the label `closedbeta::v2` is set on the customer issue.
