# Deploying the helm release

## Preparation

Go through the runbook [Preparing your local environment](./preparing-your-local-environment.md)

## Execution

1. Ensure your checkout of this repository is up-to-date with `git pull`

1. If your change is related to something out of control of Helm,
   you have to first uninstall the release, otherwise Helm will not detect a change

   ```shell
   helm --kubeconfig=./kubeconfig-orka-$CLUSTER uninstall orka-beta # or orka-canary, or gitlab-org-orka
   ```

1. Deploy the release:

   - For `orka-beta`:

    ```shell
    ./deploy.sh "$CLUSTER" beta
    ```

   - For `orka-canary`:

    ```shell
    ./deploy.sh "$CLUSTER" canary
    ```

   - For `gitlab-org-orka`:

    ```shell
    ./deploy.sh "$CLUSTER" gitlab-org-orka
    ```

1. Using k9s, verify that the pod gets scheduled and starts without problem. Look at the pod logs to see the runner booting up.

1. Generate a job on your runner using a personal project and confirm that it is passing as expected.
