# Update prometheus target

GitLab-Runner sits within Orka's `sandbox` environment. Our Prometheus setup pulls metrics
from the runner by a service configured as a `NodePort` type.

For Prometheus to access Orka's Kubernetes' nodes, the firewall is configured to port forward
to a specific node. If this node is unavailable, the target will need updating.

## Preparation

Go through the runbook [Preparing your local environment](./preparing-your-local-environment.md)

## Execution

1. Find a healthy Kubernetes Node IP.

   Listing nodes can be done with the orka command:

   ```shell
   orka <dev|prod-XX> node list
   ```

1. SSH to the firewall.

   ```shell
   ./asa.sh <dev|prod-XX>
   ```

1. Update the target to a healthy node.

   At the ASA cli:

   ```shell
   configure terminal
   object network MM001
   host <NODE_IP>
   exit
   write
   exit
   ```
