#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

# This wrapper is making sure that you connect to an Orka cluster with the same CLI version as the API version running on it.
# Additionally it helps with maintaining a set of auth tokens for the prod and dev cluster concurrently on your machine.
# Set it up in your shellrc with:
#
#  alias orka=$(path to repo)/orka-wrapper.sh
#
# Then, while connected to the `dev` vpn, with `./vpn.sh dev`:
#   orka dev config
#   orka dev login
# Switch to the `prod-XX` vpn, with `./vpn.sh prod-XX`, and:
#   orka prod-XX config
#   orka prod-XX login
# You're now logged into both environments and can execute commands with `orka dev ...` or `orka prod-XX ...`

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

CLUSTER=${1:-}
if [ "$CLUSTER" == "prod-01" ]; then
  API_ENDPOINT="http://10.221.188.100"
elif [ "$CLUSTER" == "prod-02" ]; then
  API_ENDPOINT="http://10.221.191.100"
elif  [ "$CLUSTER" == "dev" ]; then
  API_ENDPOINT="http://10.221.190.100"
else
  echo "Usage: $0 [dev prod-01 prod-02]"
  exit 1
fi
shift

API_VERSION=$(curl --silent --location --max-time 5 "$API_ENDPOINT/health-check" | jq --raw-output ".api_version")

mkdir -p "$SCRIPT_DIR/.orka-cli/$API_VERSION"
if [ ! -f "$SCRIPT_DIR/.orka-cli/$API_VERSION/orka" ]; then

  echo "Installing Orka $API_VERSION"

  TMP=$(mktemp -d)
  (
    cd "$TMP"

    OS_TYPE=$(uname -s | tr '[:upper:]' '[:lower:]')
    CLI_URL_BASE="https://cli-builds-public.s3.eu-west-1.amazonaws.com/official/$API_VERSION"

    case "${OS_TYPE}" in
        darwin)
            curl -sL "${CLI_URL_BASE}/macos/orka.pkg" -o "orka.pkg"
            xar -xf "orka.pkg"
            mv "orka-$API_VERSION.pkg/Payload"{,.gz}
            gunzip -d "orka-$API_VERSION.pkg/Payload.gz"
            cpio -i < "orka-$API_VERSION.pkg/Payload"
            ;;
        linux)
            curl -sL "${CLI_URL_BASE}/linux/orka.zip" -o "orka.zip"
            unzip orka.zip
            mkdir -p ./usr/local/bin
            mv orka ./usr/local/bin/
            find .
            ;;
        *)
            echo "Unsupported OS Type '${OS_TYPE}'"
            exit 1
    esac

  )

  mv "$TMP/usr/local/bin/orka" "$SCRIPT_DIR/.orka-cli/$API_VERSION/"

fi

# symlink the active configuration to our saved environment config
touch ~/.config/configstore/orka-cli-"$CLUSTER".json
ln -fns ~/.config/configstore/orka-cli-"$CLUSTER".json ~/.config/configstore/orka-cli.json

function cleanup {
  # unfurl the symlink so modifications made by a stock orka CLI (called without this wrapper) don't impact our saved logins
  rm ~/.config/configstore/orka-cli.json
  cp ~/.config/configstore/orka-cli-"$CLUSTER".json ~/.config/configstore/orka-cli.json
}
trap cleanup EXIT

"$SCRIPT_DIR/.orka-cli/$API_VERSION/orka" "${@}"
