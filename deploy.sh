#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

CLUSTER="${1:-}"
if [ "$CLUSTER" != "dev" ] && [ "$CLUSTER" != "prod-01" ] && [ "$CLUSTER" != "prod-02" ]; then
  echo "Please run 'deploy [dev|prod-01|prod-02] ...'"
  exit 1
fi

ENV="${2:-}"
if [ "$ENV" != "gitlab-org-orka" ] && [ "$ENV" != "canary" ] && [ "$ENV" != "beta" ]; then
  echo "Please run 'deploy [gitlab-org-orka|canary|beta] ...'"
  exit 1
fi

if [ "$ENV" == "canary" ] || [ "$ENV" == "beta" ]; then
  ENV="orka-$ENV"
fi

echo "Upgrading helm release $ENV in cluster $CLUSTER..."
helm --kubeconfig="./kubeconfig-orka-$CLUSTER" upgrade --install --namespace sandbox "$ENV" --values "values-$ENV.yaml" ./chart
