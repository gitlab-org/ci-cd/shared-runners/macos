#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

CLUSTER="${1:-}"
if [ "$CLUSTER" != "dev" ] && [ "$CLUSTER" != "prod-01" ] && [ "$CLUSTER" != "prod-02" ]; then
  echo "Please run 'backup [dev|prod-01|prod-02] ...'"
  exit 1
fi

if [ ! -f "./kubeconfig-orka-$CLUSTER" ]; then
  echo "Please get your kubeconfig for $CLUSTER saved to ./kubeconfig-orka-$CLUSTER"
  exit 1
fi

mkdir -p "backup/$CLUSTER"
kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret gcs-runners-cache-credentials -o yaml > "backup/$CLUSTER/gcs-runners-cache-credentials.yaml"
kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret runner-gitlab-org-orka -o yaml > "backup/$CLUSTER/runner-gitlab-org-orka.yaml"

if [ "$CLUSTER" == "prod-01" ] || [ "$CLUSTER" == "prod-02" ]; then
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret orka-beta -o yaml > "backup/$CLUSTER/orka-beta.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret runner-orka-beta -o yaml > "backup/$CLUSTER/runner-orka-beta.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret orka-canary -o yaml > "backup/$CLUSTER/orka-canary.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret runner-orka-canary -o yaml > "backup/$CLUSTER/runner-orka-canary.yaml"
  kubectl --kubeconfig="./kubeconfig-orka-$CLUSTER" get secret gitlab-allowlist-token -o yaml > "backup/$CLUSTER/gitlab-allowlist-token.yaml"
fi

echo "Backup completed!"
